import React, { Component } from 'react'

import {
    StyleSheet,
    Text,
    View,
    Modal,
    ScrollView,
    TextInput,
    TouchableOpacity,
  } from 'react-native'

  export default class Modal extends Component {
      render () {
          return (
              <Modal animationType="fade" transparent={true} visible={true}>
                <View style={styles.modalContainer}>
                    <View style={styles.boxContainer}></View>
                </View>
              </Modal>
          )
      }
  }

  const styles = StyleSheet.create({
      modalContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.7)',
        justifyContent: 'center',
        alignItems: 'center',
      },

      boxContainer: {
        padding: 20,
        backgroundColor: '#FFF',
        borderRadius: 10,
        alignItems: 'center',
        width: 280,
        height: 30,
      },
  })