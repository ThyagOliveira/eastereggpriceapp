O desafio consiste em uma aplicação para listagem de ovos de páscoa. No frontend, você deve 
implementar as seguintes telas:

* Tela de autenticação (usuário mockado no backend)
* Tela de listagem de todos ovos de páscoa (cards com os dados do JSON que está no backend, incluindo a imagem)
* Tela de detalhes de um ovo de páscoa (dados do JSON que está no backend, incluindo a imagem)
* Menu lateral (menu hamburger)
 * Botão para mostrar a lista de todos ovos de páscoa
 * Botão para realizar o logout da aplicação

No backend, você deve implementar os seguintes endpoints:

* POST /api/v1/auth - Login de usuário (usuário e senha mockados no próprio endpoint)
* DELETE /api/v1/auth - Logout de usuário
* GET /api/v1/easter-eggs - Retorna todos os ovos de páscoa
* GET /api/v1/easter-eggs/:egg-id - Retorna os dados de um ovo de páscoa