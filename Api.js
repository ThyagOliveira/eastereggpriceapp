import axios from 'axios'

const api = axios.create({
    baseURL:'http://localhost:3000'
})

export const loginUser = (User) => api.post ('/api/v1/auth', User)
export const logoutUser = (User) => api.delete ('/api/v1/auth', User)
export const loadEggs = () => api.get('/api/v1/easter-eggs')
export const loadEggsById = (id) => api.get('/api/v1/easter-eggs/' +id)

const apis = {
    loginUser,
    logoutUser,
    loadEggs,
    loadEggsById
}

export default apis