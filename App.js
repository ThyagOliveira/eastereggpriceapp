import React, { Component } from 'react'
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ActivityIndicator
} from 'react-native'

import Login from './Login'
import EasterEggs from './EasterEggs'
import AuthService from './AuthService'



export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoggedIn: false,
      checkingAuth: true
    }
  }
  /*componentDidMount() {
    AuthService.getAuthInfo((err, authInfo)=> {
      this.setState({
        checkingAuth: false,
        isLoggedIn: authInfo != null
      })
    })

  }*/
  render() {
    /*if(this.state.checkingAuth) {
      return(
        <View style={styles.container}>
          <ActivityIndicator
            animating={true}
            color="#333"
            size="large"
            style={styles.loader}
          /> 
        </View>
      )
    }*/
  /*  if(this.state.isLoggedIn){
      return (
        <EasterEggs />
      )
    }else {
      return (
        <Login onLogin={this.onLogin}/>
      )
    }*/
    return(
      <EasterEggs />
    )
  }
  onLogin() {
    this.setState({isLoggedIn: true})
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  loader: {
    marginTop: 20,

  },
});
