import React, { Component } from 'react'
import axios from 'axios'
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native'


import api from './Api'



export default class App extends Component {
    constructor(props){
        super(props)

        this.state = {
            eggs: [{
                id: '',
                nome: '',
                marca: '',
                sabor: '',
                peso: '',
                preco: '',
                imagem: '',
            }]
        }

    }

    componentDidMount() {
        api.loadEggs()
            .then(res => {
                this.setState({
                    eggs: res.data.results.map((egg) =>({
                        id: egg.id.toString(),
                        nome: egg.nome.toString(),
                        marca: egg.marca.toString(),
                        sabor: egg.sabor.toString(),
                        peso: egg.peso.toString(),
                        preco: egg.preco.toString(),
                        imagem: egg.imagem.toString(),

                    }))  
                })
            })   
    }

    renderEggs(eggs) {
        return(
            <View style={styles.egg}>
            
              <Image
                  style={styles.eggImage}
                  source={{ url: eggs.imagem}}
              />
              <View style={styles.eggInfo}>
                  <Text style={styles.eggName}>{eggs.id}</Text>
                  <Text style={styles.eggName}>{eggs.nome}</Text>
                  <Text style={styles.eggMarca}>{eggs.marca}</Text>
                  <Text style={styles.eggSabor}>{eggs.sabor}</Text>
                  <Text style={styles.eggPeso}>{eggs.peso}</Text>
                  <Text style={styles.eggPreco}>{eggs.preco}</Text>
              </View>
            </View>
        )
    }
    
  render() {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerText}> 
                    Seleção FFIT
                </Text>
                <TouchableOpacity onPress={() => {}}>
                    <Text style={styles.headerButton}>=</Text>
                </TouchableOpacity>
            </View>

            <ScrollView contentContainerStyle={styles.eggList}>
                {
                    this.state.eggs.map(this.renderEggs)
                }
                <View style={styles.eggs} />
                <View style={styles.eggs} />
                <View style={styles.eggs} />
            </ScrollView>
            
        </View>
        
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
  },
  header: {
    height: (Platform.OS === 'ios') ? 70 : 50,
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    backgroundColor: '#FFF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
  },

  headerButton:{
      fontSize: 24,
      fontWeight: 'bold',
  },

  headerText: {
      fontSize: 16,
      fontWeight: 'bold',
  },

  eggList: {
      padding: 20,
  },

  egg: {
    padding: 20,
    backgroundColor: '#FFF',
    height: 120,
    marginBottom: 20,
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
},

eggImage: {
    width: 50,
    height: 50,
    borderRadius: 25,

},

eggInfo: {
    marginLeft: 10,
},

eggName: {
    fontWeight: 'bold',
    color: '#333',
},

eggMarca: {
    fontSize: 12,
    color: '#999',

},
});
