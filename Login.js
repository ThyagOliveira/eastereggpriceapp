import React, { Component } from 'react';
import buffer from 'buffer'
import {
    Platform,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableHighlight,
    Button,
    ActivityIndicator,
  } from 'react-native';

import AuthService from './AuthService'

export default class Login extends Component {
    constructor(props){
        super(props)
        
        this.state = {
            username: '',
            password: '',
            showProgress: false
        }

        this.onLoginPressed = this.onLoginPressed.bind(this)
    }
   
   /* onLoginPressed() {
         console.log('Login username:' + this.state.username)
         console.log(this.state.username +' password:' + this.state.password)
         this.setState({isLogin: true})
            
         fetch('http://localhost:3000/api/v1/easter-eggs/')
         .then((response) => {
             return response.json()
         })
/*
         var b = new buffer.Buffer(this.state.username + ':' + this.state.password)
         var encodedAuth = b.toString('base64')

         fetch('http://localhost:3000', {
             headers: {
                 'Authorization': 'Basic' + encodedAuth
             }
         })
         .then((responde)=> {
             if(response.status >= 200 && resonse.status < 300) {
                 return response
             }
             if(response == 401) {
                 throw 'bad login'
             }
             throw 'Unknow error'
         })
         .then((response)=> {
             return response.json()
         })
         .then((results)=> {
             console.log(results)
             this.setState({isLogin: false})
         })
         .catch((err) => {
             console.log('logon fail' + err)
         })
         .finally(() => {
             this.setState({isLogin: false})
         })
         */

     //}
    
    render() {
        var errorCtrl = <View />

        if(!this.state.success && this.state.badCredentials){
            errorCtrl = <Text style={styles.error}>
                That username and password combination did not work
            </Text>
        }
        if(!this.state.success && this.state.unknownError){
            errorCtrl = <Text style={styles.error}>
                We experienced an unexpected issue
            </Text>
        }

        return(
            <View style={styles.container}>
                <Image style={styles.logo} ></Image>
                <Text style={styles.heading}>Login</Text>
                <TextInput 
                    style={styles.input}
                    onChangeText={(text)=> this.setState({username: text})}
                    placeholder="Username" />
                <TextInput style={styles.input}
                    placeholder="Password" 
                    onChangeText={(text) => this.setState({password: text})}
                    secureTextEntry={true}/>
                <TouchableHighlight 
                    onPress={this.onLoginPressed}
                    style={styles.button}>
                    <Text style={styles.buttonText}>Log In</Text>
                
                </TouchableHighlight>

                {errorCtrl}

                <ActivityIndicator
                    animating={this.state.showProgress}
                    color="#333"
                    size="large"
                    style={styles.loader}
                /> 

            </View>
        )
    }
    onLoginPressed() {
        this.setState({ showProgress: true })
        console.log('Login username:' + this.state.username)
        console.log(this.state.username +' password:' + this.state.password)
        console.log(this.state.showProgress)

        AuthService.login({
            username: this.state.username,
            password: this.state.password
        }, (results)=> {
            this.setState(Object.assign({
                showProgress: false
            }, results))

            if(results.success && this.props.onLogin) {
                this.props.onLogin()
            }
        })
        
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1,
        paddingTop: 40,
        alignItems: 'center',
        padding: 150
    },
    logo: {
        width: 66,
        height: 55
    },
    heading: {
        fontSize: 30,
        marginTop: 20,
    },
    input: {
        height:50,
        width: 200,
        marginTop: 20,
        padding: 4,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#48bbec',  
    },
    button:{
        height: 50,
        width: 200,
        backgroundColor: '#48BBEC',
        alignSelf: 'center',
        marginTop: 20,
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 22,
        color: '#FFF',
        alignSelf: 'center'
    },
    loader: {
        marginTop: 20,

    },

    error:{
        color: 'red',
        paddingTop: 10,
    },
})